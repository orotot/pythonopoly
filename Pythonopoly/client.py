import client_base

class Client(client_base.ClientBase):

    # Network messages received from the server
    
    def Network_dice_roll(self, data):
        die1 = data["die1"]   # value of dice
        die2 = data["die2"]
        return self._game.dice_roll(die1, die2)

    def Network_collect_salary(self, data):
        self._game.collect_salary()
        return

    def Network_demand_rent(self, data):
        index_player_demanding = data["index_player_demanding"]
        return self._game.demand_rent(index_player_demanding)

    def Network_buy_deed_just_landed(self, data):
        self._game.buy_deed_just_landed()
        return

    def Network_bid_deed_just_landed(self, data):
        index_player_bidding = data["index_player_bidding"]
        bid_increase = data["bid_increase"]
        return self._game.bid_deed_just_landed(index_player_bidding, bid_increase)

    def Network_auction_deed_just_landed(self, data):
        self._game.auction_deed_just_landed()
        return

    def Network_purchase_auctioned_deed(self, data):
        self._game.purchase_auctioned_deed()
        return

    def Network_withdraw_from_deed_auction(self, data):
        index_player_withdrawing = data["index_player_withdrawing"]
        return self._game.withdraw_from_deed_auction(index_player_withdrawing)


    def __init__(self, host, port, game):
        return super(self.__class__, self).__init__(host, port, game)
    
    # Client messages sent to server for broadcasting

    def client_dice_roll(self):
        """Generate two random numbers from 1 to 6 to sim two dice being rolled"""
        import random

        assert(self._game.index_me == self._game.index_turn)
        die1 = random.randrange(1,7)
        die2 = random.randrange(1,7)

        self.Send({"action" : "dice_roll",
                   "die1" : die1,
                   "die2" : die2})
        return

    def client_collect_salary(self):
        """Collect salary when going past GO"""
        self.Send({"action" : "collect_salary"})
        return

    def client_demand_rent(self):
        """Demand rent from the current player"""
        self.Send({"action" : "demand_rent",
                   "index_player_demanding" : self._game.index_me})

    def client_buy_deed_just_landed(self):
        """Buy deed from banker that the player has just landed on"""
        self.Send({"action" : "buy_deed_just_landed"})

    def client_bid_deed_just_landed(self, bid_increase):
        """Increase the bid on the deed being auctioned"""
        self.Send({"action" : "bid_deed_just_landed",
                   "index_player_bidding" : self._game.index_me,
                   "bid_increase" : bid_increase})

    def client_auction_deed_just_landed(self):
        self.Send({"action" : "auction_deed_just_landed"})

    def client_purchase_auctioned_deed(self):
        self.Send({"action" : "purchase_auctioned_deed"})

    def client_withdraw_from_deed_auction(self):
        self.Send({"action" : "withdraw_from_deed_auction",
                   "index_player_withdrawing" : self._game.index_me})

if __name__ == '__main__':
    host = "localhost"
    port = 8000
    c = Client(host, port, None)
    c.client_start_game()
    c.update()
    c.update()
    
