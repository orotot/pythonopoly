import pygame
import sys
import factory
import state_base
import notifications

class GameBase(object):

    """Messages returned from the server due to a 
       particular player doing something should
       be converted into data that is passed 
       into methods in this class.
       The class will contain and instantiate
       all the game objects eg Player, Board etc.
       It will then be used by the Gui class to 
       draw the objects (eg the Board, cards, token)"""

    
    def __init__(self):
        """Management class, that glues all the disparate components together"""

        # States
        self.STATE_CLIENTS_JOINING = state_base.StateClientsJoining(self)
        self.STATE_WAITING_TO_COMPLETE_TURN = state_base.StateWaitingToCompleteTurn(self)

        # Player indexes (all indexes are zero based)
        self.index_me = None   # Index of player that relates to me
        self.index_turn = None   # Index of player whose turn it is

        # Players - these are only created when you start playing
        # ie after all clients have joined and the game is started
        self.nbr_of_players = 0
        self.players = []    # All players
        self.me = None   # My Player
        self.other_players = []  # Everyone but me

        self.state = self.STATE_CLIENTS_JOINING
        
        self.gui = factory.create_gui_base(self)

        self.client = factory.create_client(self)

        self.run_loop = False

        self.notifications = notifications.Notifications("Game")

        return

    @property
    def player_turning(self): 
        return self.players[self.index_turn]

    @property
    def players_not_turning(self):
        """Get the players who are not playing the current turn"""
        return self.players[:self.index_turn] + self.players[self.index_turn+1:]

    def _start_looping(self):
        """This will start running the game update loop
           But the players haven't started playing yet.

           That will only happen when we execute start_playing()"""
        
        clock = pygame.time.Clock()

        self.run_loop = True

        # Loop until user clicks the quit button
        while self.run_loop:

            self._update()

            # This limits the while loop to a max of rr times per second.
            # Leave this out and we will use all CPU we can.
            refresh_rate = 200
            clock.tick(refresh_rate)

            # do your non-rendering game loop computation here
            # to reduce CPU usage, call this guy:
            #pygame.time.wait(100)

        return

    def _update(self):
        """Let the client send pending messages to the server,
           Refresh the GUI,
           Handle any user events"""
        self.client.update()   # two-way comm between client and server
        self.gui.update()   # gui frame refresh
        self.user_action(pygame.event.get())   # handle any events triggered by user action
        try:
            self.state.update()
        except AttributeError:
            pass
        return

    def user_action(self, events):
        """Each state will hande the user actions differently"""

        for event in events:
            # User did something
            if event.type == pygame.QUIT:
                self.client.client_quit_game()

            elif event.type == pygame.KEYDOWN:

                ctrl_pressed = False
                lshift_pressed = False
                rshift_pressed = False
                lalt_pressed = False
                ralt_pressed = False        

                mods = pygame.key.get_mods()
                if mods and mods != 4096:   # need this hack as not sure why pygame thinks 4096 mod is pressed
                    ctrl_pressed = mods & pygame.KMOD_CTRL
                    lshift_pressed = mods & pygame.KMOD_LSHIFT
                    rshift_pressed = mods & pygame.KMOD_RSHIFT
                    lalt_pressed = mods & pygame.KMOD_LALT
                    ralt_pressed = mods & pygame.KMOD_RALT

                    if lalt_pressed and event.key == pygame.K_q:
                        # Quit - Alt + Q
                        self.client.client_quit_game()
                        return events
                    elif lalt_pressed and event.key == pygame.K_s:
                        # Start - Alt + S
                        self.client.client_start_playing()
                        return events

                    if self.index_me == self.index_turn:
                        if lalt_pressed and event.key == pygame.K_h:
                            # Help - Alt + H
                            help = self.get_key_options_for_current_player()                            
                            keys = help.keys()
                            keys.sort()
                            help = [help[key] for key in keys]
                            self.me.notify(999, "Info", help)
                    else:
                        if lalt_pressed and event.key == pygame.K_h:
                            # Help - Alt + H
                            help = self.get_key_options_for_non_current_player()                    
                            keys = help.keys()
                            keys.sort()             
                            help = [help[key] for key in keys]
                            self.me.notify(999, "Info", help)
                else:
                    if self.index_me == self.index_turn:
                        if event.key == pygame.K_t:
                            # Complete (ie progress) [T]urn
                            self.client.client_complete_turn()

        return events

    def get_common_key_options_for_player(self):
        """Return a map of options available to any player"""
        ret = {"zzzAlt+H":"<Alt>+H - Help (this menu)",
               "zzzAlt+Q":"<Alt>+Q - Quit Game",
               "zzzAlt+S":"<Alt>+S - Start Game"}
        return ret

    def get_key_options_for_current_player(self):
        """Return a map of options for the turning player"""
        ret = self.get_common_key_options_for_player()
        ret.update({"T":"T - Complete Turn"})
        return ret

    def get_key_options_for_non_current_player(self):
        """Return a map of options for non-turning players"""
        ret = self.get_common_key_options_for_player()
        return ret

    def activate_state(self, new_state):
        """Set the current game state to the new_state and calls
           the on_activate() method on that state to auto run code"""

        self.state = new_state
        self.state.on_activate()
        return
        
    def notify(self, turn_nbr, message_type, message):
        self.notifications.notify(turn_nbr, message_type, message)

    def add_new_client(self, player_index):
        self.state.add_new_client(player_index)
        return

    def start_playing(self):
        self.state.start_playing()
        return

    def complete_turn(self):
        try:
            self.state.complete_turn()
        except AttributeError:
            pass
           
    def quit_game(self):        
        self.state.quit_game()

    def reset_turn_state(self):
        pass

if __name__ == '__main__':
    g = GameBase()
    g._start_looping()
