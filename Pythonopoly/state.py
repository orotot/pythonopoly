import pygame
import state_base
import factory
import inspect
from exception import GameException
from exception import PlayerException


def _pay_rent(player_demanding, game):
    """Player turning pays rent to the deed owner"""

    deed = None
    pt = game.player_turning
    pd = player_demanding
    
    try:
        deed = pt.current_square.deed                
    except AttributeError as e:
        raise PlayerException("The current player is not on a property")

    if deed.owner is pd:
        if game.rent_due:
            msg = "Rent being paid on {}".format(deed.name)
            game.notify(999, "Info", msg)
            
            try:
                pt.pay(deed.rent, pd)
            except GameException as e:
                pt.notify(999, "Warning", "You don't have enough to pay so need to sell up !")
                pd.notify(999, "Warning", "Your client doesn't have enough to pay you so must sell up !")
                raise

            game.rent_due = False
        else:
            pd.notify(999, "Warning", "Oi oi, rent was already paid mister !")
    else:
        pd.notify(999, "Warning", "Cheeky bugger, you don't own this property !")

    return

def _collect_salary(game):
    """Add 200 to player's account"""
    b = game.banker
    pt = game.player_turning

    if me is pt and game.salary_due:
        b.pay(200, pt)
        game.notify(999, "Info", "Player {} collected his salary".format(pt.idx + 1))
    else:
        raise PlayerException("You aren't due your salary !")

    return

def _does_player_have_enough_cash(player, required_cash, game):
    """Check if a player has enough after taking into account any
       existing commitments eg Auction bids"""
    cash = player.balance
    deed_auction_bidder = None
    deed_auction_bid = 0
    try:
        deed_auction_bidder = game.deed_auction_state["<HighestBidder>"]
    except KeyError:
        pass

    if deed_auction_bidder is player:
        deed_auction_bid = game.deed_auction_state["<Bid>"]

    return cash > required_cash + deed_auction_bid

def _buy_deed(buyer, deed, price, game):
    """ Buy the deed if the buyer has enough net liquid assets"""
        # Player has enough cash to buy the deed even after taking into
        # account any prior commitments

    if _does_player_have_enough_cash(buyer, price, game):
        deed.transfer_ownership(buyer, price)
    else:
        raise PlayerException("You don't have enough money to buy the deed")

    game.activate_state(game.STATE_WAITING_TO_COMPLETE_TURN, True)

    return

class _StateMonopolyBase(state_base.State):

    def __init__(self, game):
        super(_StateMonopolyBase, self).__init__(game)
        self.delay_seconds = 0
        return
    
    #def stop_delay(self):
    #    """Called when a state is activated to reset any delayed turn completion"""
    #    print "Called {} which does nothing".format(inspect.stack()[0][3])
    #    return

    def dice_rolled(self, die1, die2):
        """Called when a dice is rolled"""
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def collect_salary(self):
        """Called when a player goes past GO"""
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def demand_rent(self, player_demanding):
        """Called when a deed owner demands rent from the current player
           if he has just landed on the (unmortgaged) deed"""
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def purchase_deed(self, player_buying, deed):
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def purchase_auctioned_deed(self):
        """Purchase the deed that has just been won in the auction"""
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def purchase_house(self, player_buying, number, deed1, deed2, deed3=None):
        """Need to specify the priority of the construction using the deed1/2/3 args"""
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def auction_deed(self, deed):
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def mortgage_deed(self, player_mortgaging, deed):
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def sell_deed(self, player_selling, deed):
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def sell_house(self, player_selling, number, deed1, deed2, deed3=None):
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

    def stop_bidding(self, player_stopping):
        """Player can say he doesn't want to bid but can then restart any time
           while the auction is still going on"""
        print "Called {} which does nothing".format(inspect.stack()[0][3])
        return

class StateClientsJoining(_StateMonopolyBase, state_base.StateClientsJoining):

    def __init__(self, game):
        return super(StateClientsJoining, self).__init__(game)

    def start_playing(self):
        """Create the players, now that play has begun"""        

        state_base.StateClientsJoining.start_playing(self)

        self._game.gui = factory.create_gui(self._game)

        self._game.gui.set_caption(str(self._game.index_me + 1))

        self._game.activate_state(self._game.STATE_WAITING_TO_ROLL_DICE)
             
class StateWaitingToRollDice(_StateMonopolyBase):

    def __init__(self, game):
        super(self.__class__, self).__init__(game)
  
    def dice_rolled(self, die1, die2):
        """Dice was rolled by the current player so update the game state to reflect
        
           This includes :
           Checking current player's jail status
           Checking if doubles were thrown
           Progressing the player token
           Progressing the current player index to the next player
           Checking if player lands on a luck card, tax or jail"""
        
        g = self._game
        pt = g.player_turning

        # Salary can be collected if a player goes past GO which can be
        # determined if the new position is less than the original position
        orig_pos = pt.position
        
        double_thrown = die1 == die2
        
        if double_thrown:
            g.nbr_of_consecutive_doubles += 1
        else:
            g.nbr_of_consecutive_doubles = 0

        if double_thrown and pt.in_jail:
            print "Player {} was in jail but released due to double thrown".format(self.index_turn + 1)
            g.nbr_of_consecutive_doubles = 1
            # pt.get_out_of_jail() - this must happen on state activation
            g.activate_state(g.STATE_PLAYER_RELEASED_FROM_JAIL)
            return
        elif double_thrown and g.nbr_of_consecutive_doubles == 3:
            print "Player {} threw 3 consecutive doubles so sent to jail".format(self.index_turn + 1)
            g.nbr_of_consecutive_doubles = 0
            # pt.go_to_jail() - this must happen on state activation
            g.activate_state(g.STATE_PLAYER_SENT_DIRECTLY_TO_JAIL)
            return

        if pt.in_jail:
            pt.nbr_turns_in_jail += 1
            g.activate_state(g.STATE_WAITING_TO_COMPLETE_TURN, True)
            return
        else:
            pt.move(die1 + die2)
            print "Player {} rolled a {} and is now on {}".format(g.index_turn + 1, die1 + die2, pt.current_square.description)

            if pt.position < orig_pos and not pt.in_jail:
                g.activate_state(g.STATE_PLAYER_PASSED_GO, True)
                return

            if double_thrown:
                g.notify(999, "Info", "Double was thrown so player {} must roll again".format(g.index_turn + 1))
                        
            g.activate_state(g.STATE_PLAYER_LANDS_ON_SQUARE, True)
            return
        
        return

class StatePlayerLandsOnSquare(_StateMonopolyBase):
    """Game goes into this state after moving token"""

    def on_activate(self):
        """Player can land on :
           GO
                Player can request his salary
           Visiting Jail
           Free Parking
           Go To Jail
           Random Luck
                Pick up card and follow more instructions
           Tax
           Deed
                Player either buys the deed
                Bank auctions the deed
                Other player can request payment"""
        
        g = self._game
        banker = g.banker
        pt = g.player_turning
        sq = pt.current_square        

        g.activate_state(sq.get_state_when_player_lands(g))
        
        #if sq.can_buy_or_auction_deed(pt, banker):
        #    g.activate_state(g.STATE_PLAYER_OFFERED_DEED_PURCHASE)            
        #elif sq.is_rent_due(pt, banker):
        #    g.activate_state(g.STATE_PLAYER_OWES_RENT, True)
        #elif sq.must_go_to_jail:
        #    g.activate_state(g.STATE_PLAYER_SENT_DIRECTLY_TO_JAIL)
        #elif sq.must_pay_tax:
        #    g.activate_state(g.STATE_PLAYER_OWES_TAX)
        #else:
        #    g.activate_state(g.STATE_WAITING_TO_COMPLETE_TURN, True)
        
        return

    def demand_rent(self, player_demanding):
        return _pay_rent(player_demanding, self._game.player_turning, self._game)

class StateWaitingToCompleteTurn(_StateMonopolyBase, state_base.StateWaitingToCompleteTurn):

    #def on_activate(self):
    #    # Do nothing (overrides base class behaviour)
    #    return

    def complete_turn(self):

        state_base.StateWaitingToCompleteTurn.complete_turn(self)

        g = self._game
        g.activate_state(g.STATE_WAITING_TO_ROLL_DICE)
        
        return

    def collect_salary(self):
        _collect_salary(self._game)
        return

    def demand_rent(self, player_demanding):
        _pay_rent(player_demanding, self._game)
        return

class StateDelayTurnCompletion(_StateMonopolyBase):
    
    def __init__(self, game, underlying_state=None, delay_seconds=None):
        super(StateDelayTurnCompletion, self).__init__(game)
    
        self.delay_seconds = delay_seconds or 5
        self.remaining_seconds = self.delay_seconds
        self.underlying_state = underlying_state or game.state
        self.start_ticks = pygame.time.get_ticks()        

        print "Started waiting"

        return

    def stop_delay(self):
        if self.underlying_state:

            assert self.underlying_state.__class__ is not _StateMonopolyBase

            self._game.state = self.underlying_state
            self.underlying_state = None
            self.start_ticks = None
            print "Finished waiting"

        return

    def update(self):
        
        if self._is_time_expired():
            self.stop_delay()
                
        return

    def _is_time_expired(self):
        """Check if the time has expired"""
        if self.start_ticks:
            elapsed_seconds = (pygame.time.get_ticks() - self.start_ticks) / 1000 # calc how many seconds
            self.remaining_seconds = self.delay_seconds - elapsed_seconds

            # TODO - need to fix how the delay thing works
            # Need to auto stop delay in Update() rather than when user clicks complete turn
            # print "StartTicks {}, GetTicks {}, Elapsed {}, Remaining {}".format(self.start_ticks, pygame.time.get_ticks(), elapsed_seconds, self.remaining_seconds)
  
            return self.remaining_seconds <= 0
        
        return True

    #def _stop_if_time_expired(self):
    #    """Set the game state to the underlying state once the delay period is over"""
    #    if self._is_time_expired():
    #        self.stop_delay()
            
    def complete_turn(self):
        print "You can't complete your turn as we are waiting another {:d}s".format(self.remaining_seconds)
        return

    def on_activate(self):
        """Instructions when the current state is changed to be the 
           active game state"""
        try:
            self.underlying_state.on_activate()
        except AttributeError:
            pass
        return

    def move_token(self, die1, die2):
        self.underlying_state.dice_rolled(die1, die2)
        return

    def land_on_square(self):
        self.underlying_state.land_on_square()
        return

    def demand_rent(self, player_demanding):
        self.underlying_state.demand_rent(player_demanding)
        return

class StatePlayerOwesRent(_StateMonopolyBase):

    def on_activate(self):
        super(StatePlayerOwesRent, self).on_activate()
        game.rent_due = True

    def demand_rent(self, player_demanding):
        """Demand rent from the player turning if he has 
           landed on your property"""

        g = self._game
        try:
            _pay_rent(player_demanding, g)
        except GameException as e:
            g.activate_state(g.STATE_PLAYER_NEEDS_TO_RAISE_FUNDS)
        else:
            g.activate_state(g.STATE_WAITING_TO_COMPLETE_TURN)

        return

class StatePlayerOfferedDeedPurchase(_StateMonopolyBase):
    """Game goes into this state if a player lands on an un-owned 
       deed square.  He must choose to either buy or auction the deed"""

    def purchase_deed(self, buyer, deed, price=None):
        "Player has bought the deed and now the game must continue"

        price = price or deed.original_price
        deed.owner = deed.owner or self._game.banker
        _buy_deed(buyer, deed, price, self._game)

        self._game.activate_state(self._game.STATE_WAITING_TO_COMPLETE_TURN, True)

        return

    def auction_deed(self, deed):
        g = self._game
        bidders = set(g.players)
        auc = g.deed_auction_state
        auc["<Deed>"] = deed
        auc["<Bidders>"] = bidders
        g.activate_state(g.STATE_DEED_BEING_AUCTIONED_BY_BANK)
        return

class StateDeedBeingAuctionedByBank(_StateMonopolyBase):

    def __init__(self, game, seconds_per_bid=None):
        super(StateDeedBeingAuctionedByBank, self).__init__(game)

        self.seconds_per_bid = seconds_per_bid or 10
        self.start_ticks = None

        return

    def _is_auction_closed(self):
        """True if the time expired or all players have said 'No'"""

        if self.remaining_seconds <= 0:
            return True

        g = self._game
        auc = g.deed_auction_state
        bidders = auc["<Bidders>"]
        
        if len(bidders) <= 0:
            # Only one bidder left at most
            return True
        
        return False

    def _reset_time(self):
        self.remaining_seconds = self.seconds_per_bid
        self.start_ticks = pygame.time.get_ticks()

    def on_activate(self):
        """Set a timer going for the auction so players
           must bid within 10s or the current bidder wins the auction"""          
        self._reset_time()
        return

    def update(self):
        elapsed_seconds = (pygame.time.get_ticks() - self.start_ticks) / 1000 # calc how many seconds
        self.remaining_seconds = self.seconds_per_bid - elapsed_seconds

        if self._is_auction_closed():
            winner = None
            deed = self._game.deed_auction_state["<Deed>"]
            
            try:
                winner = self._game.deed_auction_state["<HighestBidder>"]
            except KeyError:
                self._game.notify(999, "Info", "No bids were made for {}".format(deed.name))
                self._game.deed_auction_state = {}
                self._game.activate_state(self._game.STATE_WAITING_TO_COMPLETE_TURN)
                return

            bid = self._game.deed_auction_state["<Bid>"]
            try:
                deed.transfer_ownership(winner, bid)
            except GameException:
                # Must remain in this state until winner pays for his bid !
                winner.notify(999, "Warning", "You don't have enough cash to pay for your bid so you must sell or mortgage something")
                self._game.activate_state(self._game.STATE_PLAYER_NEEDS_TO_RAISE_FUNDS)
            else:
                self._game.notify(999, "Info", "Player {} won the auction for {}".format(winner.idx + 1, deed.name))
                self._game.deed_auction_state = {}
                self._game.activate_state(self._game.STATE_WAITING_TO_COMPLETE_TURN)
                
        return

    def bid_for_deed(self, player_bidding, increment):

        # TODO - Ensure there is no race condition for bids
        
        g = self._game
        auc = g.deed_auction_state
        deed = auc["<Deed>"]
        
        bid = None
        try:
            bid = auc["<Bid>"]
        except KeyError:
            "No one in their right mind would reject buying at below mortgage price"
            bid = deed.mortgage_value
        else:
            last_bidder = auc["<HighestBidder>"]
            if player_bidding is last_bidder:
                raise PlayerException("You are already the highest bidder")
            bid = bid + increment
            
        if player_bidding.balance < bid:
            raise PlayerException("You don't have enough cash to bid for this deed")

        auc["<Bid>"] = bid
        auc["<HighestBidder>"] = player_bidding
        
        try:
            bidders = auc["<Bidders>"]
            bidders.add(player_bidding)
        except:
            pass

        # Bid made so give other players time to bid more
        self._reset_time()

        return

    def stop_bidding(self, player_stopping):
        g = self._game
        auc = g.deed_auction_state
        bidders = set(auc["<Bidders>"])
        if player_stopping in bidders:
            bidders.remove(player_stopping)

        return

    def mortgage_deed(self, player_mortgaging, deed):
        # Need to allow this to raise funds for the bid
        raise NotImplementedError()

    def sell_deed(self, player_selling, deed):
        raise NotImplementedError()

    def sell_house(self, player_selling, number, deed1, deed2, deed3=None):
        raise NotImplementedError()
    
# TODO
class StatePayingMoney(_StateMonopolyBase):
    """Game goes into this state when payment is demanded
       by either the banker (demand is automatic / compulsory)
       or by another player (when he remembers to ask)"""

    pass

class StateBuyingDeedFromPlayer(_StateMonopolyBase):
    """Game goes into this state immediately after a player 
       lands on a square after rolling the dice.

       Depending on the square's compulsory instructions (ie
       where it doesn't depend on the player explicitly
       making a request"""

    pass

class StateBuyingHouse(_StateMonopolyBase):
    """Game goes into this state immediately after  a player 
       lands ona square after rolling the dice.

       Depedning on the square's compulsory instructions (ie
       where it doesn't depend on the player explicitly
       making a request"""

    pass

class StateMortgagingDeed(_StateMonopolyBase):
    pass

class StateMortgagingHouse(_StateMonopolyBase):
    pass

class StateUnMortgagingDeed(_StateMonopolyBase):
    pass

class StateAuctionJailFreeCard(_StateMonopolyBase):
    pass

class StateAuctioningDeed(_StateMonopolyBase):
    pass



