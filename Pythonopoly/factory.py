
#def create_client(game, host = "localhost", port = 8000):
#    import client
#    return client.Client(host, port, game)

def create_client(game, host = None, port = None):
    import client_mock
    return client_mock.Client(game)


def create_server(host = "localhost", port = 8000):
    import server
    return server.Server(host, port)

def create_deeds(banker):
    import deed
    return deed.Deeds(banker)

def create_board(deeds = None):
    import board
    if deeds is None:
        deeds = create_deeds()
    return board.Board(deeds)

def create_player_token(game, player_index):
    import player    
    token_descriptions = "Car", "Ship", "Dog", "Iron", "Thimble", "Boot", "Hat", "Barrow"
    desc = token_descriptions[player_index]        
    return player.PlayerToken(game, desc)

def create_player(game, player_index, notes = None):
    import player
    if notes is None:
        notes = {500:3}
    token = create_player_token(game, player_index)
    return player.Player(game, player_index, token, notes)

def create_banker(game):
    import player
    return player.Banker(game)

def create_gui(game):
    import gui
    SQUARE_LENGTH = 70 / 2
    SQUARE_WIDTH = 60 / 2

    SQUARE_LENGTH = 55
    SQUARE_WIDTH = 45

    return gui.Gui(game, SQUARE_LENGTH, SQUARE_WIDTH)

def create_gui_base(game):
    import gui_base

    SQUARE_WIDTH = 45 * 5

    return gui_base.GuiBase(game, SQUARE_WIDTH, SQUARE_WIDTH)