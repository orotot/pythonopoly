import pygame
import game
import deed
import gui_base

def _get_square_panel_path(deed_group):

    #assert deed_group in deed.Deeds.property_groups
    
    PATH_BROWN_PANEL_IMAGE = "./monopoly_panels/property_panel_brown.jpg"
    PATH_LIGHT_BLUE_PANEL_IMAGE = "./monopoly_panels/property_panel_light_blue.jpg"
    PATH_PINK_PANEL_IMAGE = "./monopoly_panels/property_panel_pink.jpg"
    PATH_ORANGE_PANEL_IMAGE = "./monopoly_panels/property_panel_orange.jpg"
    PATH_RED_PANEL_IMAGE = "./monopoly_panels/property_panel_red.jpg"
    PATH_YELLOW_PANEL_IMAGE = "./monopoly_panels/property_panel_yellow.jpg"
    PATH_GREEN_PANEL_IMAGE = "./monopoly_panels/property_panel_green.jpg"
    PATH_DARK_BLUE_PANEL_IMAGE = "./monopoly_panels/property_panel_dark_blue.jpg"

    path = None

    if deed_group == deed.Deeds.brown:
        path = PATH_BROWN_PANEL_IMAGE
    elif deed_group == deed.Deeds.green:
        path = PATH_GREEN_PANEL_IMAGE
    elif deed_group == deed.Deeds.light_blue:
        path = PATH_LIGHT_BLUE_PANEL_IMAGE
    elif deed_group == deed.Deeds.orange:
        path = PATH_ORANGE_PANEL_IMAGE
    elif deed_group == deed.Deeds.pink:
        path = PATH_PINK_PANEL_IMAGE
    elif deed_group == deed.Deeds.dark_blue:
        path = PATH_DARK_BLUE_PANEL_IMAGE
    elif deed_group == deed.Deeds.red:
        path = PATH_RED_PANEL_IMAGE
    elif deed_group == deed.Deeds.yellow:
        path = PATH_YELLOW_PANEL_IMAGE
    
    return path

def _get_token_image_path(token_desc):

    token_descriptions = "Car", "Ship", "Dog", "Iron", "Thimble", "Boot", "Hat", "Barrow"
    
    PATH_CAR_TOKEN = "./monopoly_tokens/monopoly_token_car.png"
    PATH_SHIP_TOKEN = "./monopoly_tokens/monopoly_token_ship.png"
    
    path = None


    if token_desc == "Car":
        path = PATH_CAR_TOKEN
    elif token_desc == "Ship":
        path = PATH_SHIP_TOKEN
    
    return path

def _get_side_number_for_square(square_position):
    """Get the side where the square is located.
        GO is on side zero, Visiting Jail on one"""
    side = square_position / 10  # Zero is where the brown and ligth blues are located
    return side

def _get_side_position_for_square(square_position):
    """Gets the square's position number on its side.
        GO, Visiting Jail, Free Parking and Jail are zero.
        First Brown, Pink, Red, Green are one
        Every station is 5 etc"""
    return square_position % 10   # Each side's position will be from 0 to 9

def _get_top_left_pixels_for_square(square_position, square_length, square_width):
        """Returns an x,y pixels coordinate tuple"""
        side = _get_side_number_for_square(square_position)
        side_position = _get_side_position_for_square(square_position)

        max_xy = (square_length + 9 * square_width)

        if side == 0:
            y = 0
            if side_position == 0:
                x = 0
            else:
                x = square_length + (side_position-1) * square_width  
        elif side == 1:
            x = max_xy
            if side_position == 0:
                y = 0
            else:
                y = square_length + (side_position-1) * square_width  
        elif side == 2:
            y = max_xy
            if side_position == 0:
                x = max_xy
            else:
                # Tokens move in the -x direction for this side
                # so to make it easier, simply change side_position 
                side_position = 10 - side_position
                x = square_length + (side_position-1) * square_width 
        else:
            x = 0
            if side_position == 0:
                y = max_xy
            else:
                # Tokens move in the -y direction for this side
                # so to make it easier, simply change side_position 
                side_position = 10 - side_position
                y = square_length + (side_position-1) * square_width  

        return (x,y)

def _get_surrounding_rect_for_square(square_position, square_length, square_width):
    """Return the pygame Rect parameters in pixels"""

    side = _get_side_number_for_square(square_position)
    side_position = _get_side_position_for_square(square_position)

    dx = None
    dy = None

    if side_position in (0, 10, 20, 30):
        # Handle corner squares which are perfect squares
        dx = square_length
        dy = dx
    elif side in (0,2):
        dx = square_width
        dy = square_length
    elif side in (1,3):
        dx = square_length
        dy = square_width

    square_top_left = _get_top_left_pixels_for_square(square_position, square_length, square_width)
        
    return pygame.Rect(square_top_left, (dx, dy))
    
class Gui(gui_base.GuiBase):
    """This class just handles the drawing of components
       The player's actions are not handled here"""

    def __init__(self, game, square_length, square_width):
        
        # The board's dimension will be 9 * square_width + 2 * square_length
        self.square_length = square_length
        self.square_width = square_width
        self.board_length = 9 * square_width + 2 * square_length

        super(self.__class__, self).__init__(game, self.board_length, self.board_length)
        
        self.set_caption("Pythonopoly")
                
        # Objects that should be displayed should be set here
        # Some are automatic (eg picked chance/chest card)
        # while others will be determined by me
        self.deeds = []   # Deeds owned by me or banker or any for sale/auction or when player demands rent
        self.notes = []   # My bank notes        
        
        self._board_gui = _BoardGui(square_length, square_width, game)

        return

    def update(self):
        """Draw game objects like the 
           monopoly board with squares,
           chance and communty chest cards,
           title deed cards,
           bank notes,
           player tokens and their positions,
           player instructions,
           dice rolls,
           player choices"""

        canvas = self.canvas
        clock = self.clock

        # Clear the screen and set the screen background
        canvas.fill(gui_base.COLOR_WHITE)   

        self._board_gui.draw(canvas)
        #self._

        #self.draw_board()   # Just the empty board with squares (incl colors, desc, value)
        #self.draw_tokens()
        #self.draw_houses()
        #self.draw_hotels()
        #self.draw_dice()
        #self.draw_card_decks()
        #self.draw_next_player_sequence()        
                
        #self.draw_chance_or_chest_card()
        #self.draw_deeds()
        #self.draw_notes()

        #self.draw_query()
        #self.draw_info()

        # Go ahead and update the screen with what we've drawn.
        # This MUST happen after all the other drawing commands.
        pygame.display.flip()
 
        # This limits the while loop to a max of rr times per second.
        # Leave this out and we will use all CPU we can.
        REFRESH_RATE = 10
        clock.tick(REFRESH_RATE)

        return
    
    def turn(self):
        """We need to make sure we stop displaying items
           that are no longer relevant once the turn moves 
           to the next player"""
        super(self.__class__, self).turn()

class _BoardGui(object):
    def __init__(self, square_length, square_width, game):
        
        self.square_guis = self._init_square_guis(square_length, 
                                                  square_width, 
                                                  game.board)

        self.token_guis = self._init_token_guis(square_length, 
                                                square_width,
                                                game.players)

        return
        
    def _init_square_guis(self, square_length, square_width, board):
        """"Initialise the squares on the board"""

        square_guis = []

        for sq in board.non_deed_squares:
            sq_gui = _SquareGui(square_length, square_width, sq)
            square_guis.append(sq_gui)

        for sq in board.utility_squares:
            sq_gui = _SquareGui(square_length, square_width, sq)
            square_guis.append(sq_gui)

        for sq in board.railroad_squares:
            sq_gui = _SquareGui(square_length, square_width, sq)
            square_guis.append(sq_gui)

        for sq in board.property_squares:
            sq_gui = _DeedSquareGui(square_length, square_width, sq)
            square_guis.append(sq_gui)

        return square_guis

    def _init_token_guis(self, square_length, square_width, players):
        """Init tokens on the board"""

        token_guis = []

        for player in players:
            pt_gui = _TokenGui(square_length, square_width, player.token)
            token_guis.append(pt_gui)

        return token_guis

    def draw(self, canvas):
        for gui in self.square_guis:
            gui.draw(canvas)

        for gui in self.token_guis:
            gui.draw(canvas)

        return

class _SquareGui(object):
    """Class that can handle the drawing of a board square"""

    def __init__(self, square_length, square_width, board_square):        

        self.square_length = square_length
        self.square_width = square_width
        self.board_square = board_square
    
    def _display_text(self, canvas, rect):
        # Display text
        font = pygame.font.Font(None, gui_base.FONT_SIZE)
        desc = self.board_square.description.split()   # Splits string by space into a list

        if self.board_square.amount is not None:
            desc.append("$" + str(self.board_square.amount))
        
        side = _get_side_number_for_square(self.board_square.position)
        
        dx = 0
        dy = 0

        if side is 0:
            dy = gui_base.FONT_SIZE * 2
        elif side is 2:
            dy = gui_base.FONT_SIZE * -2
        elif side is 1:
            dx = gui_base.FONT_SIZE * -2.5
        elif side is 3:
            dx = gui_base.FONT_SIZE * 2.5

        rect = rect.move(dx, dy)

        for s in desc:
            text = font.render(s, 1, gui_base.COLOR_BLACK)
            
            # Rotate panel image depending on the side we're on
            
            if side is 0:
                rect = rect.move(0, -gui_base.FONT_SIZE)
                text = pygame.transform.rotate(text, 180)                
            elif side is 2:
                # No text rotation required
                rect = rect.move(0, gui_base.FONT_SIZE)
            elif side is 1:
                text = pygame.transform.rotate(text, 90) 
                rect = rect.move(gui_base.FONT_SIZE, 0)                   
            elif side is 3:
                text = pygame.transform.rotate(text, 270)
                rect = rect.move(-gui_base.FONT_SIZE, 0)
            
            textpos = text.get_rect()
            textpos.center = rect.center
                
            canvas.blit(text, textpos)            

    def draw(self, canvas):
        """Draw a rectangle to represent the square.
           Coordinates determined by square's position number.
           Small rectangle within large rectangle for a street square.
           Square's name about 2/3rds way up.
           Square's price about 2/3rds way down.
           So,
           Name
           Group
           Price
           Position"""

        rect = _get_surrounding_rect_for_square(self.board_square.position, self.square_length, self.square_width)

        # Draw Rect Color
        #pygame.draw.rect(canvas, self._get_square_color(), rect)
        pygame.draw.rect(canvas, gui_base.COLOR_WHITE, rect)

        # Draw outer Rect
        pygame.draw.rect(canvas, gui_base.COLOR_BLACK, rect, gui_base.LINE_THICKNESS)

        self._display_text(canvas, rect)

class _DeedSquareGui(_SquareGui):
    """Class that can handle the drawing of a board square"""

    loaded_images = {}

    def __init__(self, square_length, square_width, deed_square):        

        super(self.__class__, self).__init__(square_length, square_width, deed_square)
       
        group = deed_square.deed.group

        if group not in _DeedSquareGui.loaded_images:

            # for a deed square, we will get the panel
            image_path = _get_square_panel_path(group)

            self.image = pygame.image.load(image_path)
            self.image = pygame.transform.scale(self.image, (square_width, square_length))

            # Rotate panel image depending on the side we're on
            side = _get_side_number_for_square(self.board_square.position)
            if side is 0:
                self.image = pygame.transform.rotate(self.image, 180)
            elif side is 1:
                self.image = pygame.transform.rotate(self.image, 90)
            elif side is 3:
                self.image = pygame.transform.rotate(self.image, 270)

            _DeedSquareGui.loaded_images[group]= self.image
        
        self.image = _DeedSquareGui.loaded_images[group]

    def draw(self, canvas):
                
        super(self.__class__, self).draw(canvas)

        rect = _get_surrounding_rect_for_square(self.board_square.position, self.square_length, self.square_width)

        image_rect = self.image.get_rect()
        image_rect.center = rect.center
        
        # blit yourself at your current position
        canvas.blit(self.image, image_rect)

        self._display_text(canvas, rect)
        
class _TokenGui(_SquareGui):
    """Class that can handle the drawing of a board square"""

    loaded_images = {}

    def __init__(self, square_length, square_width, token):        

        super(self.__class__, self).__init__(square_length, square_width, token)

        self.token = token
       
        if token.description not in _TokenGui.loaded_images:

            # for a deed square, we will get the panel
            image_path = _get_token_image_path(token.description)

            self.image = pygame.image.load(image_path)
            self.image = pygame.transform.scale(self.image, (square_length / 2, square_length / 2))

            # Rotate panel image depending on the side we're on
            side = _get_side_number_for_square(token.current_square.position)
            if side is 0:
                self.image = pygame.transform.rotate(self.image, 180)
            elif side is 1:
                self.image = pygame.transform.rotate(self.image, 90)
            elif side is 3:
                self.image = pygame.transform.rotate(self.image, 270)

            _TokenGui.loaded_images[token.description]= self.image
        
        self.image = _TokenGui.loaded_images[token.description]

    def draw(self, canvas):
                
        #super(self.__class__, self).draw(canvas)

        rect = _get_surrounding_rect_for_square(self.token.current_square.position, self.square_length, self.square_width)

        image_rect = self.image.get_rect()
        image_rect.center = rect.center
        
        # blit yourself at your current position
        canvas.blit(self.image, image_rect)


