
#class MonopolyGui(object):

#    def draw_board(self, canvas):
#        board_square_guis = []        
#        for board_square in self.game.board.squares :
#            top_left = self._get_top_left_pixels_for_board_position_(board_square.position)
#            bsg = BoardSquareGui(top_left, self.square_length, self.square_width, board_square)
#            bsg.draw(canvas)

#    def draw_tokens(self, canvas):
#        """Draw each player's token on the board"""        
#        for player in self.game.players:
#            player_token = player.token
#            top_left = self._get_top_left_pixels_for_board_position_(player_token.current_square.position)
#            ptg = TokenGui(top_left, self.square_length, self.square_width, player_token)
#            ptg.draw(canvas)

#class BoardSquareGui(object):
#    """Class that can handle the drawing of a board square.
#       Will need to subclass to handle the different squares"""

#    def __init__(self, square_top_left, square_length, square_width, board_square):
        
#        MonopolyGui.validate_board_position(board_square.position)

#        self.square_top_left = square_top_left
#        self.square_length = square_length
#        self.square_width = square_width
#        self.board_square = board_square

#        image_path = self._get_square_panel_path()   
#        if image_path:             
#            #self.image = pygame.image.load(image_path)
#            self.image = pygame.transform.scale(self.image, (square_width, square_length))

#            # Rotate panel image depending on the side we're on
#            side = MonopolyGui.get_side_number_for_board_position(self.board_square.position)
#            if side is 0:
#                self.image = pygame.transform.rotate(self.image, 180)
#            elif side is 1:
#                self.image = pygame.transform.rotate(self.image, 90)
#            elif side is 3:
#                self.image = pygame.transform.rotate(self.image, 270)

#        else:
#            self.image = None
       

#    def draw(self, canvas):
#        """Draw a rectangle to represent the square.
#           Coordinates determined by square's position number.
#           Small rectangle within large rectangle for a street square.
#           Square's name about 2/3rds way up.
#           Square's price about 2/3rds way down.
#           So,
#           Name
#           Group
#           Price
#           Position"""

#        rect = self._get_surrounding_rect_()
            
#        # Load image into rect
#        if self.image:

#            image_rect = self.image.get_rect()
#            image_rect.center = rect.center
        
#            # blit yourself at your current position
#            canvas.blit(self.image, image_rect)
#        else:
#            # Draw Rect Color
#            pygame.draw.rect(canvas, self._get_square_color(), rect)

#            # Draw outer Rect
#            color = COLOR_BLACK
#            pygame.draw.rect(canvas, color, rect, LINE_THICKNESS)

#        # Display text
#        font = pygame.font.Font(None, FONT_SIZE)
#        desc = self.board_square.description.split()   # Splits string by space into a list

#        if self.board_square.amount is not None:
#            desc.append("$" + str(self.board_square.amount))
        
#        side = MonopolyGui.get_side_number_for_board_position(self.board_square.position)
        
#        dx = 0
#        dy = 0

#        if side is 0:
#            dy = FONT_SIZE * 2
#        elif side is 2:
#            dy = FONT_SIZE * -2
#        elif side is 1:
#            dx = FONT_SIZE * -2.5
#        elif side is 3:
#            dx = FONT_SIZE * 2.5

#        rect = rect.move(dx, dy)

#        #for s in desc[0],:
#        for s in desc:
#            text = font.render(s, 1, COLOR_BLACK)
            
#            # Rotate panel image depending on the side we're on
            
#            if side is 0:
#                rect = rect.move(0, -FONT_SIZE)
#                text = pygame.transform.rotate(text, 180)                
#            elif side is 2:
#                # No text rotation required
#                rect = rect.move(0, FONT_SIZE)
#            elif side is 1:
#                text = pygame.transform.rotate(text, 90) 
#                rect = rect.move(FONT_SIZE, 0)                   
#            elif side is 3:
#                text = pygame.transform.rotate(text, 270)
#                rect = rect.move(-FONT_SIZE, 0)
            
#            textpos = text.get_rect()
#            textpos.center = rect.center
                
#            canvas.blit(text, textpos)
        
#class TokenGui(BoardSquareGui):
#    """Draws the token onto a square and animates moving
#       it from departure to destination"""

#    def __init__(self, 
#                 square_top_left, 
#                 square_length, 
#                 square_width, 
#                 player_token):
#        super(self.__class__, self).__init__(square_top_left,
#                                             square_length,
#                                             square_width,
#                                             player_token.current_square)
#        self.player_token = player_token

#        image_path = self.__get_image_path_(player_token)
                
#        #self.image = pygame.image.load(image_path)
#        self.image = pygame.transform.scale(self.image, (square_width / 2, square_width / 2))
        
#    def __get_image_path_(self, player_token):
#        if player_token.desc == "Car":
#            image_path = PATH_CAR_IMAGE
#        elif player_token.desc == "Ship":
#            image_path = PATH_SHIP_IMAGE
#        elif player_token.desc == "Dog":
#            image_path = PATH_DOG_IMAGE
#        elif player_token.desc == "Iron":
#            image_path = PATH_IRON_IMAGE
#        elif player_token.desc == "Thimble":
#            image_path = PATH_THIMBLE_IMAGE
#        elif player_token.desc == "Boot":
#            image_path = PATH_BOOT_IMAGE
#        elif player_token.desc == "Hat":
#            image_path = PATH_HAT_IMAGE
#        elif player_token.desc == "Barrow":
#            image_path = PATH_BARROW_IMAGE
#        else:
#            # TODO
#            # Must raise error
#            pass        
    
#        return image_path


#    def draw(self, canvas):
#        # Load image into rect
#        rect = self._get_surrounding_rect_()
#        image_rect = self.image.get_rect()
#        image_rect.center = rect.center
#        image_rect = image_rect.move(0, self.square_width / 5)

#        # blit yourself at your current position
#        canvas.blit(self.image, image_rect)







#    def test_scratch(self):
        
#        square_length = 70
#        square_width = 60

#        g = game.Game()
#        g.start_playing(1, 0)

#        m = gui.MonopolyGui(square_length, square_width, g)

#        pygame.init()
#        canvas = pygame.display.set_mode((m.board_length, m.board_length))
#        pygame.display.set_caption("Pythonopoly")


#        #Loop until the user clicks the close button.
#        done = False
#        clock = pygame.time.Clock()
 
#        # Loop as long as done == False
#        while not done:

#            for event in pygame.event.get(): # User did something
#                if event.type == pygame.QUIT: # If user clicked close
#                    done = True # Flag that we are done so we exit this loop
#                elif event.type == pygame.MOUSEBUTTONUP:
#                   pass
 
 
#            # All drawing code happens after the for loop and but
#            # inside the main while not done loop.
     
#            # Clear the screen and set the screen background
#            canvas.fill(gui.COLOR_WHITE)
        
#            m.draw_board(canvas)

#            # Go ahead and update the screen with what we've drawn.
#            # This MUST happen after all the other drawing commands.
#            pygame.display.flip()
 
#            # This limits the while loop to a max of rr times per second.
#            # Leave this out and we will use all CPU we can.
#            refresh_rate = 10
#            clock.tick(refresh_rate)



#if __name__ == '__main__':
#    main()
