import player
import exception

class _Deed(object):
    """Represents a property or railroad or utility"""

    def __init__(self, name, group, original_price, banker):
        self.name = name
        self.group = group
        self.original_price = original_price
        self.mortgage_value = original_price / 2.0

        self.is_mortgaged = False
        self.owner = banker
        self.banker = banker

    def mortgage(self, banker):
        """Mortgage the deed and give money to the owner's account"""
        assert self.owner is not banker
        assert not self.is_mortgaged 

        banker.pay(self.owner, self.mortgage_value)
        self.is_mortgaged is True

        return

    def un_mortgage(self, banker):
        "Un-mortgage the deed and pay the banker his due"""

        assert self.owner is not banker
        assert self.is_mortgaged 

        required_amount = deed.mortgage_value * 1.1   # 10% charge for un-mortgaging deed
        
        self.owner.pay(banker, required_amount)
        deed.is_mortgaged is False
        
        return

    def transfer_ownership(self, new_owner, new_market_price = None):
        """New owner is buying the deed from the current owner
           If a new price isn't provided, will use the orig deed price"""

        assert self.owner is not None

        orig_owner = self.owner

        if new_market_price is None:
            new_market_price = self.original_price

        new_owner.pay(new_market_price, self.owner)
        self.owner = new_owner

        if orig_owner is not self.banker:
            orig_owner.notify(999, "Info", "You just sold {}".format(self.name))

        new_owner.notify(999, "Info", "You just bought {}".format(self.name))
        return

class Property(_Deed):
    
    """Street property where you can build houses and hotels"""
    def __init__(self, 
                 name,
                 group, 
                 original_price,
                 owner,
                 house_purchase_price,
                 rental_yields):
        """rental_yields -> {NbrOfHouses : Rental}"""
        super(self.__class__, self).__init__(name, group, original_price, owner)
        self.house_purchase_price = house_purchase_price
        self.rental_yields = rental_yields
        self._nbr_of_buildings = 0
       
    def rent(self):
        assert self._nbr_of_buildings >= 0 and self._nbr_of_buildings <= 5
        return self.rental_yields[self._nbr_of_buildings]
     
    @property
    def houses(self):
        """Four houses max"""
        assert self._nbr_of_buildings <= 5

        if self._nbr_of_buildings == 5:
            # We don't have any houses then, just a hotel
            ret = 0
        else:
            ret = self._nbr_of_buildings
        return ret

    @property
    def hotels(self):
        """Five houses equals a hotel"""
        assert self._nbr_of_buildings <= 5
        if self._nbr_of_buildings == 5:
            ret = 1
        else:
            ret = 0
        return ret

    def build(self, deeds):
        """Builds one more house"""

        assert self._nbr_of_buildings < 5
        
        # Check if the same owner owns all properties in the group
        # and that we don't build by more than one cf other props in group
        group_properties =  deeds.grouped_deeds(self.group)
        for gp in group_properties:
            assert self.owner is gp.owner
            assert self._nbr_of_buildings <= gp.houses

        self.owner.pay(self.banker, self.house_purchase_price)
        self._nbr_of_buildings += 1

class Railroad(_Deed):
    """Rental yield is dependant on how many railroads you own in total"""
    def __init__(self, name, owner):
        super(self.__class__, self).__init__(name, Deeds.railroad, 200, owner)
        return

    def rent(self, game_railroads, banker):
        rents = {0:0, 1:25, 2:50, 3:100, 4:200}
        owner = self.owner
        if owner in (None, banker):
            # deed doesn't have an owner so no rent is due on it
            return 0
        player_deeds = set(owner.deeds)
        owned_railroads = set(game_railroads).intersection(player_deeds)
        return rents[len(owned_railroads)]

class Utility(_Deed):
    """Yield is dpendant on how many utilities you own plus dice roll"""
    def __init__(self, name, owner):
        super(self.__class__, self).__init__(name, Deeds.utility, 150, owner)

    def rent(self, dice_value, game_utilities, banker, got_here_by_luck = False):
        """Rent is 4 or 10 times dice value
           If same owner for all utilities or we got here by
           pickin up a luck card, it's 10 else it's 4"""

        if self.owner in (None, banker):
            # deed doesn't have an owner so no rent is due on it
            multiplier = 0
        else:
            multiplier = 10

            if got_here_by_luck is False:
                for utility in game_utilities:
                    if utility.owner is not self.owner:
                        multiplier = 4
                        break

        return dice_value * multiplier
        
class Deeds(object):
    """Keeps track of all Deeds"""

    # Enumerate the groups
    groups = range(10)
    brown, light_blue, pink, orange, red, yellow, green, dark_blue, utility, railroad = groups

    def __init__(self, banker = None):
        self.properties = []
        self.utilities = []
        self.railroads = []
        self.all = []

        self._init_properties(banker)
        self._init_railroads(banker)
        self._init_utilities(banker)

        self.all.extend(self.properties)
        self.all.extend(self.utilities)
        self.all.extend(self.railroads)

        return

    def grouped_deeds(self, deed_group):
        """Get all deeds in the same 'color' group"""
        ret = []
        if deed_group == Deeds.utility:
            ret = self.utilities
        elif deed_group == Deeds.railroad:
            ret = self.railroads
        elif deed_group in (Deeds.brown, Deeds.light_blue, Deeds.pink, Deeds.orange, Deeds.red, Deeds.yellow, Deeds.green, Deeds.dark_blue):            
            ret = [prop for prop in self.properties if prop.group == deed_group]
        else:
            raise exception.GameException("Unknown deed group {}".format(deed_group))
                
        return ret
            
    def _init_properties(self, banker):
        """Create all the deeds available in the game"""
        
        # id, name, group, orig_price, house_price, basic_rent
        prop_details = (("Mediterranean Avenue", Deeds.brown, 60, 50, 2),
                        ("Baltic Avenue", Deeds.brown, 60, 50, 4),
                        ("Oriental Avenue", Deeds.light_blue, 100, 50, 6),
                        ("Vermont Avenue", Deeds.light_blue, 100, 50, 6),                
                        ("Connecticut Avenue", Deeds.light_blue, 120, 50, 8),
                        ("St. Charles Place", Deeds.pink, 140, 100, 10),
                        ("States Avenue", Deeds.pink, 100, 140, 10),                
                        ("Virginia Avenue", Deeds.pink, 160, 100, 12),
                        ("St. James Place", Deeds.orange, 180, 100, 14),
                        ("Tennessee Avenue", Deeds.orange, 180, 100, 14),                
                        ("New York Avenue", Deeds.orange, 200, 100, 16),
                        ("Kentucky Avenue", Deeds.red, 220, 150, 18),
                        ("Indiana Avenue", Deeds.red, 220, 150, 18),                
                        ("Illinois Avenue", Deeds.red, 240, 150, 20),
                        ("Atlantic Avenue", Deeds.yellow, 260, 150, 22),
                        ("Ventnor Avenue", Deeds.yellow, 260, 150, 22),                
                        ("Marvin Gardens", Deeds.yellow, 280, 150, 24),
                        ("Pacific Avenue", Deeds.green, 300, 200, 26),
                        ("North Carolina Avenue", Deeds.green, 300, 200, 26),                
                        ("Pennsylvania Avenue", Deeds.green, 320, 200, 28),
                        ("Park Place", Deeds.dark_blue, 350, 200, 35),
                        ("Board Walk", Deeds.dark_blue, 400, 200, 50))

        
        for det in prop_details:
            self.properties.append(Property(det[0], det[1], det[2], banker, det[3], det[4]))
    
        # TODO - do this dynamically !
        index = 0
        self.property_brown1 = self.properties[index]; index += 1
        self.property_brown2  = self.properties[index]; index += 1
        self.property_light_blue1  = self.properties[index]; index += 1
        self.property_light_blue2  = self.properties[index]; index += 1
        self.property_light_blue3  = self.properties[index]; index += 1
        self.property_pink1  = self.properties[index]; index += 1
        self.property_pink2  = self.properties[index]; index += 1
        self.property_pink3  = self.properties[index]; index += 1
        self.property_orange1  = self.properties[index]; index += 1
        self.property_orange2  = self.properties[index]; index += 1
        self.property_orange3  = self.properties[index]; index += 1
        self.property_red1  = self.properties[index]; index += 1
        self.property_red2  = self.properties[index]; index += 1
        self.property_red3  = self.properties[index]; index += 1
        self.property_yellow1  = self.properties[index]; index += 1
        self.property_yellow2  = self.properties[index]; index += 1
        self.property_yellow3  = self.properties[index]; index += 1
        self.property_green1  = self.properties[index]; index += 1
        self.property_green2  = self.properties[index]; index += 1
        self.property_green3  = self.properties[index]; index += 1
        self.property_dark_blue1  = self.properties[index]; index += 1
        self.property_dark_blue2  = self.properties[index]

    def _init_railroads(self, banker):
        """Create all the deeds available in the game"""
        
        # id, name, group, orig_price, house_price, basic_rent
        rr_details = ("Reading Railroad", "Pennsylvania Railroad", "B&O Railroad", "Shortline Railroad")

        
        for det in rr_details:
            self.railroads.append(Railroad(det, banker))
    
        # TODO - do this dynamically !
        index = 0
        self.railroad1 = self.railroads[index]; index += 1
        self.railroad2 = self.railroads[index]; index += 1
        self.railroad3 = self.railroads[index]; index += 1
        self.railroad4 = self.railroads[index]; index += 1
     
    def _init_utilities(self, banker):
        """Create all the deeds available in the game"""
        
        util_details = ("Electric Company", "Water Works")
        
        for det in util_details:
            self.utilities.append(Utility(det, banker))
    
        # TODO - do this dynamically !
        index = 0
        self.utility1 = self.utilities[index]; index += 1
        self.utility2 = self.utilities[index]; index += 1

    def __iter__(self):
        return iter(self.all)