import unittest
import factory
import time
import sys
import Queue
import threading        

class Test_Game(unittest.TestCase):

    def setUp(self):

        super(Test_Game, self).setUp()

        def start_server(que):
            svr = factory.create_server()
            que.put(svr)
            svr.start_running()

        que = Queue.Queue()

        thread = threading.Thread(target=start_server, args=(que,))

        thread.start()
        self.server = que.get()

    def tearDown(self):
        try:
            self.server.stop_running()
        except SystemExit:
            pass

        self.assertFalse(self.server.client_channels)

        return

    def test_connect_client_to_server(self):
        """Given a Server that is waiting for Clients to connect
           When I start up a new Client
           Then the Server will register me as a new Client
           And the Server will notify all Clients that I have connected
           And my Gui will draw a Board with Tokens for each player"""

        self.assertFalse(self.server.client_channels)

        # Creating a game will connect the client to 
        # the server and also start the update loop
        # that draws the pygame gui
        game1 = factory.create_game()
        game1.connect_client_to_server()
        # There are two messages being sent when a client connects so call update() twice
        game1.client.update()
        game1.client.update()
        game1.client.update()

        #self.assertEqual(1, len(self.server.client_channels))
        #self.assertEqual(1, game1.client.nbr_of_players)

        #print "ZZZ after game 1 {} ZZZ".format(self.server.client_channels)

        game2 = factory.create_game()
        game2.connect_client_to_server()
        game2.client.update()
        game2.client.update()
        game2.client.update()
        
        print "ZZZ after game 2 {} ZZZ".format(self.server.client_channels)

        #self.assertEqual(2, len(self.server.client_channels))
        #self.assertEqual(2, game1.client.nbr_of_players)
        #self.assertEqual(2, game2.client.nbr_of_players)

        return

    def xtest_new_client_connects_to_server(self):
        """Given a Server that I have already registered with
           When a new Client starts a Game
           Then the Server will register the new Client
           And the Server will notify all Clients that a new Client has connected
           And my Gui will draw a Board with Tokens for each player"""

        self.assertTrue(False)
        pass

    def xtest_start_playing_game(self):
        """Given a Server with Clients
           When the first Client clicks 'Begin'
           Then the Server notifies all Clients that teh Game has begun
           And the Turn is set to the first Client"""
        self.assertTrue(False)
        pass

    def xtest_player_moves_token(self):
        """Given a Player isn't in jail
           When he roll the Dice
           And it's not a a third double
           Then move the token"""
        self.assertTrue(False)
        pass

    def xtest_player_sent_to_jail_after_three_doubles(self):
        """Given a Player playing his Turn
           When he roll three doubles in the same Turn
           Then he will be sent to Jail
           And he will not collect his salary"""
        self.assertTrue(False)
        pass

    def xtest_player_throws_double(self):
        """Given a Player rolls the Dice
           When it's a double
           And it's not his third double in this Turn
           Then he will move his Token
           And the Turn will still be his"""
        self.assertTrue(False)
        pass

    def xtest_player_completes_his_turn(self):
        """Given a Player who has rolled the dice
           When he hasn't rolled a double
           Or he isn't sent to Jail
           Then the Turn is set to the next Player"""
        self.assertTrue(False)
        pass

    def xtest_player_spends_three_turns_in_jail(self):
        """Given a Player has previously spent two Turns in Jail
           When he gets the third Turn in Jail
           Then he must use his GetOutOfJail card
           Or Pay the Jail Fee
           And he must move his Token"""

        self.assertTrue(False)
        pass

if __name__ == '__main__':
    unittest.main()
