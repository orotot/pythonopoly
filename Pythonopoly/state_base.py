import pygame
import factory

class State(object):
    
    def __init__(self, game):
        """Defined the current state of the Game and the 
           possible operations allowed"""

        self._game = game

        return
    
    def update(self):
        # do nothing
        return

    #def on_activate(self):
    #    """Instructions when the current state is changed to be the 
    #       active game state"""
    #    print "Called {} which does nothing".format(inspect.stack()[0][3])

    def quit_game(self):
        print "Quitting game"
        self._game.run_loop = False
        return

    #def complete_turn(self):
    #    print "Called {} which does nothing".format(inspect.stack()[0][3])
    #    self._game.player_turning.notifications.append("You can't complete your turn just yet")
    #    return

class StateClientsJoining(State):

    def add_new_client(self, index_new_player):
        """When a new client connects to the server, all the games
           are notified.  
           Its possible that the new connection is for this game
           itself in which case we set index_me as well"""
         
        nth = None  
        if index_new_player in range(0,3):
            nth = ("1st", "2nd", "3rd")[index_new_player]
        else:
            nth = "{}th".format(index_new_player+1)

        if self._game.index_me is None:
            self._game.index_me = index_new_player            
            print "You are the {} player to join the game".format(nth)
        else:
            print "The {} player has joined the game".format(nth)

        self._game.nbr_of_players = index_new_player + 1
        return

    def start_playing(self):
        """Create the players, now that play has begun"""        

        for i in range(0, self._game.nbr_of_players):
            new_player = factory.create_player(self._game, i)

            self._game.players.append(new_player)
            if i == self._game.index_me:
                self._game.me = new_player                
            else:
                self._game.other_players.append(new_player)

        PLAYER_INDEX_TO_START = 0   # Player id zero will have the first turn
        self._game.index_turn = PLAYER_INDEX_TO_START

        if self._game.index_me == self._game.index_turn:
            print "Play has begun and it's your turn, good luck"
        else:
            print "Play has begun, good luck"
            
        self._game.state = self._game.STATE_WAITING_TO_COMPLETE_TURN
     
class StateWaitingToCompleteTurn(State):
    
    def _get_next_player_index(self):
        """Set the next player explicitly to the passed in next_player_index 
           if provided.
           If next_player_index is None, we automatically set to the next
           player in the list of players"""

        index_turn = self._game.index_turn
        next_player_index = index_turn + 1

        next_player_index = next_player_index % self._game.nbr_of_players

        return next_player_index

    def on_activate(self):
        pass

    def complete_turn(self):
        """Set the turn to be for the next player"""
        self._game.index_turn = self._get_next_player_index()
        
        print ""
        print self._game.me.info
        print ""
        print "Turn completed and now it's player {}'s turn".format(self._game.index_turn + 1)            
        
        return