import datetime
        
class Notifications(object):
    def __init__(self, context):
        self.context = context
        self.notifications = {}  # Key=Turn, Value = [(time, message_type, message]

    def notify(self, turn_nbr, message_type, message):
        """Add message to player notifications"""

        now = datetime.datetime.now()
        item = now, message_type, message
        turn_items = None
        try:
            turn_items = self.notifications[turn_nbr]
        except KeyError:
            turn_items = [("Time", "MessageType", "Message")]

        turn_items.append(item)
        self.notifications[turn_nbr] = turn_items

        if not isinstance(message, basestring):
            message = "\n\t".join([""] + message)

        print "{} {} - {}".format(self.context, message_type, message)
