import unittest
import account

class Test_Account(unittest.TestCase):
    
    def test_get_balance(self):
        ac = account.Account({500.0 : 2,
                              100 : 4.0,
                              50 : 1,
                              20 : 1,
                              10.0 : 2.0,
                              5 : 1,
                              1 : 5})
        self.assertEqual(1500, ac.balance)

        return

    def test_get_notes_for_payment(self):
        ac = account.Account({500 : 2,
                              100 : 4,
                              50 : 1,
                              20 : 1,
                              10 : 2,
                              5 : 1,
                              1 : 5})
        expected = {500 : 3,
                    100 : 2,
                    50 : 0,
                    20 : 2,
                    10 : 0,
                    5 : 0,
                    1 : 1}
        self.assertDictEqual(expected, ac._get_notes_for_payment(1741))

        expected = {500 : 0,
                    100 : 0,
                    50 : 0,
                    20 : 0,
                    10 : 0,
                    5 : 0,
                    1 : 1}
        self.assertDictEqual(expected, ac._get_notes_for_payment(1))
        
        return

    def test_break_note(self):

        ac = account.Account({500 : 3})

        ac._break_notes()
        self.assertEqual(1500, ac.balance)

        expected = {500 : 2,
                    100 : 4,
                    50 : 1,
                    20 : 1,
                    10 : 2,
                    5 : 1,
                    1 : 5}

        self.assertDictEqual(expected, ac.notes)

    def test_transfer(self):
        # Break 500s to get lots of change and then tfr the 1234
        # Simulates taking money from banker
        ac_from = account.Account({500:1000})
        ac_to = account.Account({500:0})
        ac_from.transfer(1234, ac_to)
        self.assertEqual(1234, ac_to.balance)

        # Break 500 to get lots of change and then tfr the 5r
        ac_from = account.Account({500:1})
        ac_to = account.Account({500:0})
        ac_from.transfer(5, ac_to)
        self.assertEqual(495, ac_from.balance)
        self.assertEqual(5, ac_to.balance)

        # We need a 5 but dont have.  
        # So we try to break a 10 but we don't have that either
        # Then we break a 20 into a 10 and a 5 and 5 ones
        # Then we pay out the 5
        ac_from = account.Account({20:1})
        ac_to = account.Account({500:0})
        ac_from.transfer(5, ac_to)
        self.assertEqual(15, ac_from.balance)
        self.assertEqual(5, ac_to.balance)

        # We need 4 ones but dont have.  
        # So we try to break a 5, then 10 but we don't have those either
        # Then we break a 20 into a 10 and a 5 and 5 ones
        # Then we pay out the 5
        ac_from = account.Account({20:1})
        ac_to = account.Account({500:0})
        ac_from.transfer(4, ac_to)
        self.assertEqual(16, ac_from.balance)
        self.assertEqual(4, ac_to.balance)
    
if __name__ == '__main__':
    unittest.main()
