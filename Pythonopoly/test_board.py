import unittest
import mock
import board
import game
import deed
import player

class Test_Board(unittest.TestCase):

    @mock.patch("game.Game")
    def test_create_board_with_squares(self, mock_game):
        """Given I have a new Monopoly game
           When the board is created
           Then there will be 40 squares on the board
           And their positions will run from 0 to 39
           And each square will appear only once
           And the zeroth square will be Go
           And the second square will be Community Chest"""           
      
        b = board.Board(mock_game)

        # Check that Go is on square zero
        self.assertEqual(0, b.square_go.position)
        self.assertEqual(2, b.square_community_chest_1.position)

        # Check we have 40 squares in total
        self.assertEqual(40, len(b.squares))
        
        # Check we have every position filled on the board (0 to 39)
        i = 0
        for square in b.squares:
            self.assertEqual(i, square.position)
            i = i + 1

        # Check we haven't populated the same square twice (ie all are unique)
        self.assertEqual(len(b.squares), len(set(b.squares)))

        return
    
    def test_can_buy_or_auction_deed(self):
        """Given a deed square where the deed is not owned
           When I land on the square
           I can buy or auction that deed"""

        b = mock.MagicMock("player.Banker")
        pt = mock.MagicMock("player.Player")
        
        d = deed.Property("Brown1", "brown", 30, b, 5, {1:5})
        sq = board._DeedSquare(1, d)

        type(pt).current_square = mock.PropertyMock(return_value=sq)

        self.assertTrue(sq._can_buy_or_auction_deed(pt, b))

        """Given a deed square where the deed is already owned
           When I land on the square
           I can't buy or auction that deed"""

        # Deed is owned by another player (not player turning)
        po = mock.MagicMock("player.Player")
        d.owner = po

        self.assertFalse(sq._can_buy_or_auction_deed(pt, b))

        return

    def test_is_rent_due_when_player_lands_on_deed_square(self):
        """Given a deed owned by another player
           When the player turning lands on it's square
           Then rent is due to be paid on that deed"""

        pt = mock.MagicMock("player.Player")   # turning player
        b = mock.MagicMock("player.Banker")    # banker
        po = mock.MagicMock("player.Player")   # (an)other player

        # Deed is owned by another player (not player turning)            
        d = deed.Property("Brown1", "brown", 30, po, 5, {1:5})
        sq = board._DeedSquare(1, d)

        type(pt).current_square = mock.PropertyMock(return_value=sq)
        
        self.assertTrue(sq._is_rent_due(pt, b))

        """Given a deed owned by the player turning
           When the player turning lands on it
           Then rent is not due to be paid on that deed"""
        d.owner = pt
        self.assertFalse(sq._is_rent_due(pt, b))

        return

if __name__ == '__main__':
    unittest.main()
