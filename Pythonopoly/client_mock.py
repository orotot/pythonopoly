import math
from time import sleep

class Client(object):
    def __init__(self, game):
        self._game = game
        self._game.add_new_client(0)

    def update(self):
        sleep(0.1)
    
    def client_start_playing(self):
        """One-off event to begin the game for everyone"""
        self._game.start_playing()

    def client_quit_game(self):
        """Let all the players know to quit the game"""
        self._game.quit_game()
        return
    
    def client_complete_turn(self):
        """Progress the turn to the next player"""
        self._game.complete_turn()
        return

    def client_dice_roll(self):
        """Generate two random numbers from 1 to 6 to sim two dice being rolled"""
        import random

        die1 = random.randrange(1,7)
        die2 = random.randrange(1,7)

        self._game.dice_roll(die1, die2)
        return

    def client_collect_salary(self):
        self._game.collect_salary()
        return

    def client_demand_rent(self):
        """Demand rent from the current player"""
        self._game.demand_rent(self._game.index_me)
        return

    def client_buy_deed_just_landed(self):
        self._game.buy_deed_just_landed()
        return

    def client_bid_deed_just_landed(self, bid_increase):
        index_player_bidding = self._game.index_me
        self._game.bid_deed_just_landed(index_player_bidding, bid_increase)
        return

    def client_auction_deed_just_landed(self):
        self._game.auction_deed_just_landed()
        return

    def client_purchase_auctioned_deed(self):
        self._game.purchase_auctioned_deed()
        return

    def client_withdraw_from_deed_auction(self):
        index_player_withdrawing = self._game.index_me
        self._game.withdraw_from_deed_auction(index_player_withdrawing)
        return
