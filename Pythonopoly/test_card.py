import unittest
import card

class Test_Cards(unittest.TestCase):
    def test_shuffle(self):
        new_pack = (card._Card(), card._Card(), card._Card(), card._Card(), card._Card)
        cards = card.Cards(new_pack)
        
        for i, c in enumerate(cards.cards):
            self.assertEqual(i, c.id)

        cards.shuffle()
        self.assertEqual(len(new_pack), len(cards.cards))

        # Test is a bit flaky if we have the unlikely chance that shuffling
        # the cards kept them in the same order
        order_is_the_same = True
        for i, c in enumerate(cards.cards):
            if c.id != i:
                order_is_the_same = False
                break

        self.assertFalse(order_is_the_same)
        return

    def test_pickup_and_return_card(self):
        new_pack = (card._Card(), card._Card(), card._Card(), card._Card(), card._Card)
        cards = card.Cards(new_pack)

        c = cards.pick_top_card()
        self.assertEqual(0, c.id)
        self.assertEqual(len(new_pack)-1, len(cards.cards))

        cards.return_card(c)
        self.assertEqual(len(new_pack), len(cards.cards))
        self.assertEqual(0, cards.cards[len(new_pack)-1].id)
        

        

if __name__ == '__main__':
    unittest.main()
