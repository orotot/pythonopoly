import pygame
import sys
import factory
import state
import game_base

from exception import GameException
from exception import PlayerException

class Game(game_base.GameBase):

    """Messages returned from the server due to a 
       particular player doing something should
       be converted into data that is passed 
       into methods in this class.
       The class will contain and instantiate
       all the game objects eg Player, Board etc.
       It will then be used by the Gui class to 
       draw the objects (eg the Board, cards, token)"""

    def __init__(self):
        """Management class, that glues all the disparate components together"""

        super(self.__class__, self).__init__()

        # States
        self.STATE_CLIENTS_JOINING = state.StateClientsJoining(self)
        self.STATE_WAITING_TO_ROLL_DICE = state.StateWaitingToRollDice(self)        
        self.STATE_PLAYER_LANDS_ON_SQUARE = state.StatePlayerLandsOnSquare(self) 
        self.STATE_PLAYER_OFFERED_DEED_PURCHASE = state.StatePlayerOfferedDeedPurchase(self)
        self.STATE_WAITING_TO_COMPLETE_TURN = state.StateWaitingToCompleteTurn(self)   # overrides base class property
        self.STATE_PLAYER_OWES_RENT = state.StatePlayerOwesRent(self)
        self.STATE_PAYING_RENT = state.StatePayingMoney(self)
        self.STATE_DEED_BEING_AUCTIONED_BY_BANK = state.StateDeedBeingAuctionedByBank(self)   # players have 5 seconds to bid more
        
        # Need to be implemented
        self.STATE_PLAYER_PASSED_GO = state._StateMonopolyBase(self)      # player just passed go so can collect salary
        self.STATE_PLAYER_SENT_TO_JAIL = state._StateMonopolyBase(self)   # sent to jail but can collect salary
        self.STATE_PLAYER_SENT_DIRECTLY_TO_JAIL = state._StateMonopolyBase(self)   # sent to jail and can't collect salary
        self.STATE_PLAYER_RELEASED_FROM_JAIL = state._StateMonopolyBase(self)   # doesn't pay jail fee
        self.STATE_PLAYER_COMPLETES_JAIL_SENTENCE = state._StateMonopolyBase(self)   # pays jail fee
        self.STATE_PLAYER_OWES_TAX = state._StateMonopolyBase(self)   # must pay tax
        self.STATE_PLAYER_NEEDS_TO_RAISE_FUNDS = state._StateMonopolyBase(self)   # player needs to pay but doesn't have enough dough
        self.STATE_PLAYER_PICKING_LUCK_CARD = state._StateMonopolyBase(self)   # player needs to pick up the chance or community chest card
        
        
        # TODO - not used yet
        self.STATE_SELLING_DEED_TO_PLAYER = state.StatePlayerOfferedDeedPurchase(self)
        self.STATE_BUYING_HOUSE = state.StateBuyingHouse(self)
        self.STATE_MORTGAGING_DEED = state.StateMortgagingDeed(self)
        self.STATE_MORTGAGING_HOUSE = state.StateMortgagingHouse(self)
        self.STATE_UNMORTGAGING_DEED = state.StateUnMortgagingDeed(self)
        self.STATE_AUCTIONING_JAIL_CARD = state.StateAuctionJailFreeCard(self)
        self.STATE_AUCTIONING_DEED = state.StateAuctioningDeed(self)
        self.STATE_PAYING_MONEY = state.StatePayingMoney(self)
        
        
        self.state = self.STATE_CLIENTS_JOINING

        self.banker = factory.create_banker(self)
        self.deeds = factory.create_deeds(self.banker)
        self.board = factory.create_board(self.deeds)
        
        self.rent_due = False
        self.salary_due = False

        self.deed_auction_state = {}   # Keys are : <Deed>, <Bid>, <HighestBidder>, <Bidders>
        self.nbr_of_consecutive_doubles = 0   # Dependant on player thrown doubles
        
        return        

    def reset_turn_state(self):
        """Reset any turn based state"""

        super(self.__class__, self).reset_turn_state()

        self.salary_due = False
        self.rent_due = False
        self.nbr_of_consecutive_doubles = 0
        self.deed_auction_state = {}
        self.current_bid = None

        return

    def user_action(self, events):
        """"Handle game specific events here"""

        events = super(self.__class__, self).user_action(events)

        if not events:
            return

        for event in events:
            # User did something
            if event.type == pygame.KEYDOWN:

                BID_INCREMENT = 10   # Used when bidding on an Auction

                # Events that apply to all players

                if event.key == pygame.K_b:
                    #  [B]id for deed if in an auction
                    self.client.client_bid_deed_just_landed(BID_INCREMENT)
                    return events
                elif event.key == pygame.K_p:
                    # [P]ay for deed won in auction
                    self.client.client_purchase_auctioned_deed()
                    return events
                elif event.key == pygame.K_w:
                    # [W]ithdraw from auction bid
                    self.client.client_withdraw_from_deed_auction()
                    return events

                if self.index_me == self.index_turn:

                    # Process player action that can only be done
                    # during his/her turn
                    # Could have done this with states but was 
                    # easier to just have an if statement here                    
                    if event.key == pygame.K_a:
                        # [A]uction deed just landed
                        self.client.client_auction_deed_just_landed()
                        return events
                    elif event.key == pygame.K_b:
                        # [B]uy deed just landed
                        self.client.client_buy_deed_just_landed()                        
                        return events
                    elif event.key == pygame.K_c:
                        # [C]ollect Salary
                        self.client.client_collect_salary()
                    elif event.key == pygame.K_m:
                        # [M]ortgage deed
                        return events
                    elif event.key == pygame.K_r:
                        # [R]oll dice if it's my turn
                        self.client.client_dice_roll()
                    elif event.key == pygame.K_s:
                        # [S]ell deed or house or get-out-of-jail card
                        return events
                    elif event.key == pygame.K_u:
                        # [U]nmortgage deed
                        return events
                else:
                    if event.key == pygame.K_d:
                        #  [D]emand payment 
                        self.client.client_demand_rent()
                        return events             
                        
        return events

    def get_common_key_options_for_player(self):
        """Return a map of options available to any player"""
        ret = super(Game, self).get_common_key_options_for_player()
        ret.update({"B":"B - Bid for deed in an auction",
                    "P":"P - Pay",
                    "W":"W - Withdraw from an auction"})
        return ret

    def get_key_options_for_current_player(self):
        """Return a map of options for the turning player"""
        ret = super(Game, self).get_key_options_for_current_player()
        ret.update({"A":"A - Auction deed",
                    "B":"B - Buy deed",
                    "C":"C - Collect salary",
                    "M":"M - Mortgage deed (TBD)",
                    "R":"R - Roll dice",
                    "S":"S - Sell deed or house (TBD)",
                    "U":"U - Unmortgage deed"})
        return ret

    def get_key_options_for_non_current_player(self):
        """Return a map of options for non-turning players"""
        #ret = self.get_common_key_options_for_player()
        ret = super(Game, self).get_key_options_for_non_current_player()
        ret.update({"D":"D - Demand rent (TBD)"})
        return ret
      
    def activate_state(self, 
                       new_state, 
                       delay_turn=False, 
                       stop_current_delay = True):
        """Set the current game state to the new_state and calls
           the on_activate() method on that state to auto run code.

           If delay_turn is true, we ensure the next player can't 
           roll the dice for 5 seconds, thus giving other players
           to demand payment etc"""

        assert new_state is not None
        assert new_state.__class__ is not state._StateMonopolyBase

        orig_state = self.state
        
        if stop_current_delay:
            try:
                self.state.stop_delay()
            except AttributeError:
                pass

        if delay_turn:
            self.state = state.StateDelayTurnCompletion(self, new_state)
        else:
            self.state = new_state

        try:
            self.state.on_activate()
        except AttributeError:
            pass

        if self.state.__class__ is state.StateDelayTurnCompletion:
            print "Game state is {} and underlying is {}".format(self.state.__class__.__name__, self.state.underlying_state.__class__.__name__)
        else:
            print "Game state is {}".format(self.state.__class__.__name__)

        return

    def dice_roll(self, die1, die2):
        """Dice was rolled by the current player so update the game state to reflect
        
           This includes :
           Checking current player's jail status
           Checking if doubles were thrown
           Progressing the player token
           Progressing the current player index to the next player
           Checking if player lands on a luck card, tax or jail"""
        
        # Move the token if it needs to 
        # Also determine if the turn should move to the next player
        try:
            self.state.dice_rolled(die1, die2)
        except AttributeError:
            pass
        except GameException as e:
            self.notify(999, "Warning", e.message)

        return

    def collect_salary(self):
        try:
            self.state.collect_salary()
        except AttributeError:
            pass
        except GameException as e:
            self.notify(999, "Warning", e.message)

        return

    def demand_rent(self, index_player_demanding):
        pd = self.players[index_player_demanding]
        try:
            self.state.demand_rent(pd)
        except AttributeError:
            pass
        return

    def buy_deed_just_landed(self):
        buyer = self.player_turning
        deed = None
        
        try:
            deed = buyer.current_square.deed            
        except AttributeError:
            return

        try:
            self.state.purchase_deed(buyer, deed)
        except AttributeError:
            pass
        except GameException as e:
            self.notify(999, "Warning", e.message)

        return
        
    def bid_deed_just_landed(self, index_player_bidding, bid_increase):
        #pt = self.player_turning
        #try:
        #    deed = pt.current_square.deed            
        #except AttributeError:
        #    return

        bidder = self.players[index_player_bidding]        
        try:
            self.state.bid_for_deed(bidder, bid_increase)
        except AttributeError:
            pass
        except GameException as e:
            self.notify(999, "Warning", e.message)
        except PlayerException as e:
            bidder.notify(999, "Warning", e.message)

        return

    def auction_deed_just_landed(self):
        pt = self.player_turning
        try:
            deed = pt.current_square.deed            
        except AttributeError:
            return

        try:
            self.state.auction_deed(deed)
        except AttributeError:
            pass
        except GameException as e:
            self.notify(999, "Warning", e.message)
        
        return

    def withdraw_from_deed_auction(self, index_player_withdrawing):
        pw = self.players[index_player_withdrawing]
        try:
            self.state.withdraw_from_deed_auction(pw)
        except AttributeError:
            pass
        except GameException as e:
            self.notify(999, "Warning", e.message)
        except PlayerException as e:
            pw.notify(999, "Warning", e.message)

if __name__ == '__main__':
    g = Game()
    g._start_looping()
