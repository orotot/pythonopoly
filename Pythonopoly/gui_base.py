import pygame
import game
import deed

# Define the colors we will use in RGB format
COLOR_BLACK = (  0,  0,    0)
COLOR_WHITE = (255, 255, 255)
COLOR_BLUE =  (  0,   0, 255)
COLOR_GREEN = (  0, 255,   0)
COLOR_RED =   (255,   0,   0)

COLOR_BROWN =      (200, 100,  20)
COLOR_LIGHT_BLUE = ( 40, 180, 220)
COLOR_PINK =       (240,  90, 220)
COLOR_ORANGE =     (230, 150,  40)
COLOR_YELLOW =     (220, 230,  40)
COLOR_PURPLE =     (170,  40, 230)

LINE_THICKNESS = 1
FONT_SIZE = 12

class GuiBase(object):
    """This class just handles the drawing of components
       The player's actions are not handled here"""

    def __init__(self, game, length, width):
        
        self.game = game

        # The board's dimension will be 9 * square_width + 2 * square_length
        self.length = length
        self.width = width

        self.query = None   # Queries that require a player response (eg do they want to purchase deed?)
        self.info = None   # Notifications to other players of my actions

        pygame.init()
        self.canvas = pygame.display.set_mode((self.width, self.length))   # first arg is a tuple hence double brackets
        pygame.display.set_caption("My Game")
        self.clock = pygame.time.Clock()

        return

    def set_caption(self, caption):
        pygame.display.set_caption(caption)  

    def update(self):
        """Draw game objects like the 
           monopoly board with squares,
           chance and communty chest cards,
           title deed cards,
           bank notes,
           player tokens and their positions,
           player instructions,
           dice rolls,
           player choices"""

        canvas = self.canvas
        clock = self.clock

        # Clear the screen and set the screen background
        canvas.fill(COLOR_WHITE)   

        # Go ahead and update the screen with what we've drawn.
        # This MUST happen after all the other drawing commands.
        pygame.display.flip()
 
        # This limits the while loop to a max of rr times per second.
        # Leave this out and we will use all CPU we can.
        REFRESH_RATE = 10
        clock.tick(REFRESH_RATE)

        return
    
    def turn(self):
        """We need to make sure we stop displaying items
           that are no longer relevant once the turn moves 
           to the next player"""
        self.query = None
        self.info = None        

