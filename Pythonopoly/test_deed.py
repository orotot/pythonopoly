import unittest
import deed
import player
import game
import mock

class Test_Deed(unittest.TestCase):

    def test_railroad_rent(self):
        """Given a railroad deed
           When no player owns that deed or it is owned by the banker
           Then the rent is $0"""

        ds = deed.Deeds()   # deeds
        b = mock.MagicMock("player.Banker")
        p1 = mock.MagicMock("player.Player", deeds=[ds.railroad1, ds.property_brown1])
        p2 = mock.MagicMock("player.Player", deeds=[ds.railroad3, ds.railroad4])
        ds.property_brown1.owner = p1
        ds.railroad1.owner = p1
        ds.railroad3.owner = p2
        ds.railroad4.owner = p2
        rr = ds.railroad2
        rent = rr.rent(ds.railroads, b)
        self.assertEqual(0, rent)
        ds.railroad2.owner = b
        rent = rr.rent(ds.railroads, b)
        self.assertEqual(0, rent)

        """Given a railroad deed
           When the deed's owner only owns that one railroad
           Then the rent is $25"""
        rr = ds.railroad1    
        rent = rr.rent(ds.railroads, b)
        self.assertEqual(25, rent)

        """Given a railroad deed
           When the deed's owner also owns another railroad
           Then the rent is $50"""
        rr = ds.railroad4
        rent = rr.rent(ds.railroads, b)
        self.assertEqual(50, rent)       

        return

    def test_utility_rent(self):
        """Given a utility deed
           When no player owns that deed or it is owned by the banker
           Then the rent is $0"""

        ds = deed.Deeds()   # deeds
        b = mock.MagicMock("player.Banker")
        p1 = mock.MagicMock("player.Player", deeds=[ds.railroad1, ds.property_brown1])
        p2 = mock.MagicMock("player.Player", deeds=[ds.railroad3])
        ds.property_brown1.owner = p1
        ds.railroad1.owner = p1
        ds.railroad3.owner = p2
        u1 = ds.utility1
        rent = u1.rent(2, ds.utilities, b)
        self.assertEqual(0, rent)
        rent = u1.rent(2, ds.utilities, b, True)
        self.assertEqual(0, rent)        

        """Given a utility deed
           When a player owns that utility only
           And a player hasn't arrived on that utility by picking a luck card
           Then the rent is 4 times the dice value"""
        p2 = mock.MagicMock("player.Player", deeds=[ds.railroad3, ds.utility1])
        ds.railroad3.owner = p2
        ds.utility1.owner = p2
        rent = u1.rent(2, ds.utilities, b)
        self.assertEqual(8, rent)        
        
        """Given a utility deed
           When a player owns that utility only
           But the player has arrived on that utility by picking a luck card
           Then the rent is 10 times the dice value"""
        p1 = mock.MagicMock("player.Player", deeds=[ds.utility1])
        p2 = mock.MagicMock("player.Player", deeds=[ds.utility2])
        ds.utility1.owner = p1
        ds.utility2.owner = p2
        rent = u1.rent(2, ds.utilities, b, True)
        self.assertEqual(20, rent)        
        
        """Given a utility deed
           When a player owns both utilities
           Then the rent is 10 times the dice value"""
        p1 = mock.MagicMock("player.Player", deeds=[ds.railroad1, ds.property_brown1])
        p2 = mock.MagicMock("player.Player", deeds=[ds.utility1, ds.utility2])
        ds.utility1.owner = p2
        ds.utility2.owner = p2
        rent = u1.rent(2, ds.utilities, b, False)
        self.assertEqual(20, rent)
        
        return

    def test_get_green_properties(self):
        """Given a list of deeds in the game
           When the green deeds are requested
           Then the green deeds are returned"""

        deeds = deed.Deeds()
        greens = set(deeds.grouped_deeds(deed.Deeds.green))
        self.assertSetEqual(set([deeds.property_green1, deeds.property_green2, deeds.property_green3]), greens)

        """Given a list of deeds in the game
           When the utilities are requested
           Then the utilities are returned"""
        utils = set(deeds.grouped_deeds(deed.Deeds.utility))
        self.assertSetEqual(set([deeds.utility1, deeds.utility2]), utils)


if __name__ == '__main__':
    unittest.main()
