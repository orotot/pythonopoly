import account
import notifications

class _PlayerBase(object):

    def __init__(self, game):
        
        self.game = game
        self.account = None

    def pay(self, amount, payee):
        assert self.account is not None

        return self.account.transfer(amount, payee.account)
    
    @property
    def deeds(self):
        """Deeds owned by player"""
        my_deeds = []
        for deed in self.game.deeds:
            if deed.owner is self:
                my_deeds.append(deed)

        return my_deeds

class Banker(_PlayerBase):
    """Special player for banker"""

    def __init__(self, game):
        super(self.__class__, self).__init__(game)

        self.account = account.Account({500:1000})
       
class Player(_PlayerBase):
    """To simulate the actions of a player or 
       the banker during a game of Monopoly"""
    
    def __init__(self, game, idx, token, notes):
        
        super(self.__class__, self).__init__(game)

        self.idx = idx
        self.token = token
        self.account = account.Account(notes)

        self.nbr_turns_in_jail = None
        self.needs_to_pay_jail_fee = None

        self.get_out_of_jail_chance_cards = []
        self.get_out_of_jail_chest_cards = []

        self.notifications = notifications.Notifications("Player{}".format(idx))

        
    @property
    def info(self):
        """Player's state"""
        details = {'Player' : '{} ({})'.format(self.idx+1, self.token.description),
                   'Position' : '{} ({})'.format(self.position, self.current_square.description),
                   'Balance' : self.balance}

        if self.deeds:
            deed_names = []
            for deed in self.deeds:
                deed_name = deed.name
                if deed.is_mortgaged:
                    deed_name = deed_name + "*"
                deed_names.append(deed_name)

            details['Deeds'] = ','.join(deed_names)

        if self.in_jail:
            details['TurnsInJail'] = self.nbr_turns_in_jail
            
        return details

    @property
    def position(self): 
        return self.token.current_square.position

    @property
    def current_square(self):
        return self.token.current_square

    @property
    def balance(self):
        return self.account.balance

    @property
    def in_jail(self):
        """Is the player serving a jail sentence?"""
        return self.nbr_turns_in_jail is not None

    def notify(self, turn_nbr, message_type, message):
        self.notifications.notify(turn_nbr, message_type, message)

    def get_out_of_jail(self):
        """Player is released from jail.
           This function doesn't handle payment of release fee"""           
        self.nbr_turns_in_jail = None        
        return

    def go_to_jail(self):
        """Send player to jail"""
        self.nbr_turns_in_jail = 0

    def move(self, nbr_of_squares):
        return self.token.move(nbr_of_squares)    

class PlayerToken(object):
    """A player's token keeps track of its own position on the board"""

    def __init__(self, game, description):
        self.description = description
        self._board = game.board        
        self.current_square = self._board.square_go        

    def move(self, nbr_of_squares):
        """Move the token to a new square"""

        # Genearlly, token can move between 2 and 12 spaces except
        # one of the chance cards asks player to moved back 3 spaces
        if nbr_of_squares == -3 or (nbr_of_squares > 1 and nbr_of_squares <= 12):
            new_position = self.current_square.position + nbr_of_squares
            if new_position > 39:
                new_position = new_position - 40
            assert(new_position >=0 and new_position < 40)
            self.current_square = self._board.squares[new_position]
        elif nbr_of_squares > 12:
            raise ValueError("Can only move a maximum of 12 squares at any one go")
        else:
            raise ValueError("Can only move forward up to twelve or back three spaces")
