import unittest
import player
import deed
import board
import game
import mock

class Test_Player(unittest.TestCase):

    @mock.patch("game.Game")
    @mock.patch("deed._Deed")
    def test_player_offers_to_buy_deed_from_bank(self, mock_deed, mock_game):
        """Given a deed
           When the deed is available for sale from the bank
           And I have sufficient funds to pay the asking price           
           Then I will pay the original price to the bank
           And I will own the deed"""

        p = player._PlayerBase(mock_game)

        pass

    def test_player_chooses_to_buy_a_deed(self):
        """Given the chance to buy a deed
           When I have sufficient funds
           And I agree to buy the deed
           Then I will pay the requested price for the deed
           And the deed's ownership will transfer to me"""
        pass
           
    def test_deed_ownership(self):            
                
        g = game.Game()
        b = player.Banker(g)
        deeds = deed.Deeds(b)
        g.deeds = deeds
        p = player._PlayerBase(g, {500:3})

        # owned_deeds()
        self.assertFalse(p.owned_deeds(deeds))
        self.assertListEqual(deeds.all, b.owned_deeds(deeds))
        
        # buy_deed()
        # Add the 'as <object>' in case we want to inspect the exception
        # Kept here just for example purposes, not actually used
        with self.assertRaises(AssertionError) as raises_assertion:
            b.request_buy_deed(p, deeds.property_brown2)

        # owns_deed()
        # buy_deed()
        # owned_deeds()
        self.assertTrue(b.owns_deed(deeds.property_brown1))
        self.assertFalse(p.owns_deed(deeds.property_brown1))
        p.request_buy_deed(b, deeds.property_brown1)
        self.assertEqual([deeds.property_brown1], p.owned_deeds(deeds))
        self.assertFalse(b.owns_deed(deeds.property_brown1))
        self.assertTrue(p.owns_deed(deeds.property_brown1))        

        pass

    def test_pay_cash(self):
        
        g = game.Game()
        b = player.Banker(g)        
        self.assertEqual(1023 * 500, b.account.balance())
        
        # pay()
        p = player._PlayerBase(g)
        b.pay(p, 500)

        self.assertEqual(1022 * 500, b.account.balance())
        self.assertEqual(500, p.account.balance())

        return

    def test_token_movement(self):
       
        g = game.Game()
        g.start_playing(1,0)
        p = g.me
        g.play_turn()

        # move_token()
        self.assertEqual(p.token.current_square, g.board.square_go)
        p.move_token(12)
        self.assertEqual(p.token.current_square, g.board.square_electric_company)

        # go_to_jail()
        # move_token()
        p.go_to_jail()
        self.assertEqual(p.token.current_square, g.board.square_jail)
        with self.assertRaises(AssertionError) as raises_assertion:
            p.move_token(4)        

        # pay_jail_fee()
        # get_out_of_jail()
        # move_token()
        self.assertEqual(p.token.current_square, g.board.square_jail)
        p.request_pay_jail_fee(0)
        p.get_out_of_jail()        
        p.move_token(4)
        self.assertEqual(p.token.current_square, g.board.square_pink3)

        return

    def test_can_collect_salary(self):
        
        g = game.Game()
        g.start_playing(1,0)
        p = g.me

        self.assertFalse(p.can_collect_salary)
        
        p.move_token_to(g.board.square_yellow2)
        p.move_token(12)
        self.assertFalse(p.can_collect_salary)
        
        p.move_token_to(g.board.square_water_works)
        p.move_token(12)
        self.assertTrue(p.can_collect_salary)
        
        p.move_token_to(g.board.square_dark_blue1)
        p.move_token(5)
        self.assertTrue(p.can_collect_salary)
        
        p.move_token_to(g.board.square_go_to_jail)
        self.assertFalse(p.can_collect_salary)

    def test_move_token(self):
        
        banker = player.Banker()
        b = board.Board(deed.Deeds(banker))
        t = player.PlayerToken("Car", b)

        self.assertEqual(0, t.current_square.position)        

        t = player.PlayerToken("Car", b, b.square_chance_3)
        self.assertEqual(36, t.current_square.position)
        
        t.move(2)
        self.assertEqual(38, t.current_square.position)

        t.move(-3)
        self.assertEqual(35, t.current_square.position)
        
        t.move(10)
        self.assertEqual(5, t.current_square.position)        

        self.assertRaises(ValueError, t.move, -1)
        self.assertRaises(ValueError, t.move, 1)
        self.assertRaises(ValueError, t.move, 13)
        
    
    # TODO
    def xtest_pick_up_luck_card(self):
        pass

    # TODO
    def xtest_mortgage_deed(self):
        pass

    # TODO
    def xtest_property_buildings(self):
        pass

    # TODO
    def xtest_jail(self):
        pass

    # TODO

if __name__ == '__main__':
    unittest.main()

