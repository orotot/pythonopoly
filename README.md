# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Create a multi-player Monopoly game in Python for fun
* Main point of this project is to learn new stuff and enjoy myself
* For example, learn how to :
* - create a server and register multiple game clients
* - create a simple gui (say using pygame but this should be loosely coupled)
* - try out different designs and find one that works well for turn based games in general
* - learn / familiarise myself with a pythonic programming style


### How do I get set up? ###

* Download the source and install pygame 2.7
* Run game.py
* Press Alt+S to start the game (on the square gui window).  This will show the monopoly board and your token
* Press Alt+H to see the available keys on the cmd window
* EG: Press R to roll the dice (the token will move on gut board gui)
* If you land on a property, press A to auction it or B to buy it
* If auctioning, press  B to bid on the auction
* Press T to complete your turn